# Desenvolvimento de um Quadro de Kanban

Trilha de Backend

Stack:
* NodeJS (v18.17.1)
* TypeScript
* ExpressJS
* Sequelize
  - Driver: Sqlite3
* Jest
* Supertest

## Funcionalidades

* [x] Autenticação
* [x] Autorização
* [x] _Cards_
  - [x] Inserir _card_
  - [x] Editar _card_
  - [x] Excluir _card_
  - [x] Log de quando Altera ou Deleta um _card_

## Executando testes

```
yarn test
```

ou

```
npm test
```

## Executando aplicação

### Etapa 1: Configure o arquivo `.env`

As variáveis disponíveis são:

```
DB_URI=sqlite::memory: # sequelize uri
JWT_SECRET=da2sk.7fl3s # secret usada para gerar/validar o token
AUTH_USER=letscode # usuário que será usado para autenticação
AUTH_PASS=lets@123 # senha que será usado para autenticação
```

### Etapa 2: Instalação dos pacotes

```bash
yarn
```

ou

```
npm install
```

### Etapa 3: Iniciando a aplicação

```bash
yarn dev
```

ou

```
npm run dev
```

## Gerando *release*

```
yarn build
```

ou

```
npm run build
```

O resultado final fica na pasta `dist`
