import { Optional } from "sequelize";
import { dbSync } from "../../../src/database/init";
import Card, {
  CardInputAttributes,
  CardState,
} from "../../../src/database/models/Card";
import { request } from "../testServer";
import { generateJwtToken } from "../../../src/helpers/jwtToken";

const validJwtToken = generateJwtToken({ user: "Test" });

let seed: Optional<CardInputAttributes, "id">[] = [
  {
    titulo: "sample title",
    conteudo: "sample text",
    lista: "ToDo",
  },
  {
    titulo: "sample title",
    conteudo: "sample text",
    lista: "Doing",
  },
  {
    titulo: "sample title",
    conteudo: "sample text",
    lista: "ToDo",
  },
  {
    titulo: "sample title",
    conteudo: "sample text",
    lista: "Done",
  },
];

describe("Cards", () => {
  beforeAll(async () => {
    await setupDatabase();
  });

  it("should return an empty array", async () => {
    const { body: data } = await request
      .get("/cards")
      .auth(validJwtToken, { type: "bearer" })
      .expect(200);
    expect(data).toHaveLength(seed.length);
  });

  it("should return not found", async () => {
    await request
      .get("/cards/not_exists_id")
      .auth(validJwtToken, { type: "bearer" })
      .expect(404);
  });

  it.each`
    titulo     | conteudo
    ${"Teste"} | ${null}
    ${null}    | ${"Conteudo"}
  `("should try create a new card", async ({ titulo, conteudo }) => {
    await request
      .post("/cards")
      .auth(validJwtToken, { type: "bearer" })
      .send({
        titulo,
        conteudo,
      })
      .expect(400);
  });

  it("should create a new card", async () => {
    const { body: data } = await request
      .post("/cards")
      .auth(validJwtToken, { type: "bearer" })
      .send({
        titulo: "Teste 1",
        conteudo: `
      # Titulo

      ## SubTitulo

      Texto
      `,
      });

    seed.push(data);

    expect(data.id).not.toBeNull();
    expect(data.lista).toBe(CardState.ToDo);
    expect(data.titulo).toBe("Teste 1");
    expect(data.conteudo).toContain("Texto");
  });

  it("should try retrieve a specific card by id", async () => {
    await request
      .get(`/cards/not_exists_id`)
      .auth(validJwtToken, { type: "bearer" })
      .expect(404);
  });

  it("should retrieve a specific card by id", async () => {
    const { body: data } = await request
      .get(`/cards/${seed[0].id}`)
      .auth(validJwtToken, { type: "bearer" })
      .expect(200);

    expect(data.titulo).toBe("sample title");
  });

  it("should try update a specific card by id", async () => {
    await request
      .put(`/cards/not_exists_id`)
      .auth(validJwtToken, { type: "bearer" })
      .send({
        lista: "Doing",
      })
      .expect(404);
  });

  it("should update a specific card by id", async () => {
    const currentProps = seed[seed.length - 1];

    const { body: data } = await request
      .put(`/cards/${currentProps.id}`)
      .auth(validJwtToken, { type: "bearer" })
      .send({
        ...currentProps,
        lista: "Doing",
      });

    expect(data.lista).toBe("Doing");
  });

  it("should try delete a specific card by id", async () => {
    await request
      .delete(`/cards/not_exists_id`)
      .auth(validJwtToken, { type: "bearer" })
      .expect(404);
  });

  it("should delete a specific card by id", async () => {
    const currentProps = seed[seed.length - 1];

    await request
      .delete(`/cards/${currentProps.id}`)
      .auth(validJwtToken, { type: "bearer" })
      .expect(200);
  });

  describe("expects 401 (Unthorized)", () => {
    it("[GET] /cards", async () => {
      await request.get("/cards").expect(401);
    });

    it("[GET] /cards/:id", async () => {
      await request.get(`/cards/${seed[0].id}`).expect(401);
    });

    it("[POST] /cards", async () => {
      await request
        .post("/cards")
        .send({
          titulo: "sample title",
          conteudo: "sample text",
          lista: "ToDo",
        })
        .expect(401);
    });

    it("[PUT] /cards/:id", async () => {
      await request
        .put(`/cards/${seed[0].id}`)
        .send({
          lista: "Doing",
        })
        .expect(401);
    });

    it("[DELETE] /cards/:id", async () => {
      await request.delete(`/cards/${seed[0].id}`).expect(401);
    });
  });
});

async function setupDatabase() {
  await dbSync();

  await Card.bulkCreate(seed);

  seed = await Card.findAll();
}
