import { request } from "../testServer";
import {
  generateJwtToken,
  verifyJwtToken,
} from "../../../src/helpers/jwtToken";
import { delay } from "../../../src/helpers/utils";

describe("Auth", () => {
  it("should try authenticate using malformed input", async () => {
    await request.post("/login").send({}).expect(400);
  });

  test.each`
    login                    | password                 | expected | isValidJwt
    ${"letscode"}            | ${"invalid"}             | ${401}   | ${false}
    ${"invalid"}             | ${"lets@123"}            | ${401}   | ${false}
    ${process.env.AUTH_USER} | ${process.env.AUTH_PASS} | ${200}   | ${true}
  `(
    "returns $expected when try authenticate using login: $login and pass: $password",
    async ({ login, password, expected, isValidJwt }) => {
      const { body: token } = await request
        .post("/login")
        .send({
          login: login,
          senha: password,
        })
        .expect(expected);

      expect(verifyJwt(token)).toBe(isValidJwt);
    }
  );

  it("expect 403 (Forbidden)", async () => {
    const expiredToken = generateJwtToken(
      { user: "Expired user" },
      {
        expiresIn: "1s",
      }
    );

    await delay(1000);

    await request
      .get("/cards")
      .auth(expiredToken, { type: "bearer" })
      .expect(403);
  }, 2000);
});

const verifyJwt = (token) => {
  try {
    verifyJwtToken(token);
    return true;
  } catch {
    return false;
  }
};
