import { agent as createTestServer } from "supertest";
import { createApplication } from "../../src/api";

const app = createApplication();
export const request = createTestServer(app);
