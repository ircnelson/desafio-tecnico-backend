import type { Config } from "@jest/types";

const config: Config.InitialOptions = {
  detectOpenHandles: true,
  verbose: true,
  coveragePathIgnorePatterns: ["/node_modules/"],
  transform: {
    "^.+\\.ts?$": "ts-jest",
  },
};
export default config;
