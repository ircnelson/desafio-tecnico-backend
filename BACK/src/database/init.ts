import sequelize from ".";
import { databaseSettings } from "../settings";

export const dbSync = () =>
  sequelize.sync({
    force: databaseSettings.forceSync,
  });
