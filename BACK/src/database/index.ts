import { Sequelize } from "sequelize";
import { databaseSettings } from "../settings";

const sequelize = new Sequelize(databaseSettings.uri, {
  logging: false,
});

export default sequelize;
