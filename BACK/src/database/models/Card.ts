import { DataTypes, Model, Optional, UUIDV4 } from "sequelize";
import sequelize from "..";

interface CardAttributes {
  id: string;
  titulo: string;
  conteudo: string;
  lista: CardState | string;
}

export type CardInputAttributes = Optional<CardAttributes, "id">;

export enum CardState {
  ToDo = "ToDo",
  Doing = "Doing",
  Done = "Done",
}

class Card
  extends Model<CardAttributes, CardInputAttributes>
  implements CardAttributes
{
  id: string;
  titulo: string;
  conteudo: string;
  lista: CardState | string;
}

Card.init(
  {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
      defaultValue: UUIDV4
    },
    titulo: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    conteudo: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    lista: {
      type: DataTypes.ENUM<CardState>(
        CardState.ToDo,
        CardState.Doing,
        CardState.Done
      ),
      allowNull: false,
      defaultValue: CardState.ToDo,
    },
  },
  {
    sequelize: sequelize,
    tableName: "card",
  }
);

export default Card;
