require("dotenv").config();

interface DatabaseSettings {
  uri: string;
  forceSync: boolean;
}

interface AuthSettings {
  login: string;
  password: string;
  jwtSecret: string
}

export const databaseSettings = {
  uri: process.env.DB_URI,
  forceSync: process.env.NODE_ENV != 'production',
} as DatabaseSettings;

export const authSettings = {
  login: process.env.AUTH_USER,
  password: process.env.AUTH_PASS,
  jwtSecret: process.env.JWT_SECRET
} as AuthSettings;
