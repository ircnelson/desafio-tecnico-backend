import { createApplication } from "./api";
import { dbSync } from "./database/init";

const port = process.env.PORT || 5000;

const app = createApplication();

try {
  dbSync().then(() => {
    app.listen(port, () => {
      console.log(`Server is running on port: http://localhost:${port}`);
    });
  });
} catch (error) {
  console.error(`An error occurred: ${error}`);
}
