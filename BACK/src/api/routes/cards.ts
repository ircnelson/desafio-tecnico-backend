import { Request, Response, Router } from "express";
import Card, { CardInputAttributes } from "../../database/models/Card";
import { randomUUID } from "crypto";
import { LogOperation, createLogOperation } from "../../helpers/utils";

const cardsRouter = Router();

cardsRouter.get("/", async function (_: Request, res: Response) {
  const cards = await Card.findAll();

  return res.json(cards);
});

cardsRouter.get("/:id", async function (req: Request, res: Response) {
  const { id } = req.params;

  const card = await Card.findByPk(id);

  if (!card) {
    return res.sendStatus(404);
  }

  return res.status(200).json(card);
});

cardsRouter.post("/", async function (req: Request, res: Response) {
  const payload: CardInputAttributes = req.body;

  if (!payload.titulo || !payload.conteudo) {
    return res.sendStatus(400);
  }

  const card = await Card.create({
    ...payload,
    id: randomUUID(),
  });

  return res.status(201).json(card);
});

cardsRouter.put(
  "/:id",
  withLogging(async function (req: Request, res: Response) {
    const payload: CardInputAttributes = req.body;

    const { id } = req.params;
    const card = await Card.findByPk(id);

    if (!card) {
      return res.sendStatus(404);
    }

    await card.update(payload);

    return res.status(200).json(card);
  })
);

cardsRouter.delete(
  "/:id",
  withLogging(async function (req: Request, res: Response) {
    const { id } = req.params;

    const card = await Card.findByPk(id);

    if (!card) {
      return res.sendStatus(404);
    }

    await card.destroy();

    const cards = await Card.findAll();

    return res.status(200).json(cards);
  })
);

function withLogging(handler: (req: Request, res: Response) => Promise<any>) {
  return async function (req: Request, res: Response) {
    await handler(req, res);

    if (res.statusCode === 200) {
      const id = req.params.id;
      const { titulo } = req.body;

      const message = `Card ${id} - ${titulo}`;

      const logOperation = createLogOperation(
        req.method === "PUT" ? LogOperation.Alter : LogOperation.Delete
      );

      console.log(logOperation(message));
    }
  };
}

export default cardsRouter;
