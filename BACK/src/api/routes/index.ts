import { Router } from "express";
import cardsRouter from "./cards";
import authRouter, { onlyAuthenticated } from "./auth";

const router = Router();

router.use('/login', authRouter);
router.use('/cards', onlyAuthenticated, cardsRouter);

export default router;
