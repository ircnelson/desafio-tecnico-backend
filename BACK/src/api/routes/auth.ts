import { NextFunction, Request, Response, Router } from "express";
import { authSettings } from "../../settings";
import { generateJwtToken, verifyJwtToken } from "../../helpers/jwtToken";

const authRouter = Router();

authRouter.post("/", (req: Request, res: Response) => {
  const { login, senha } = req.body;

  if (!login || !senha) {
    return res.sendStatus(400);
  }

  if (login == authSettings.login && senha == authSettings.password) {
    const token = generateJwtToken(
      { user: login },
      {
        expiresIn: "600s",
      }
    );

    return res.json(token).status(200);
  }

  return res.sendStatus(401);
});

const sanitizeToken = (headerValue: string) =>
  headerValue.replace(/[Bb]earer /, "");

export const onlyAuthenticated = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.headers.authorization) {
    return res.sendStatus(401);
  }

  try {
    const token = sanitizeToken(req.headers.authorization);
    verifyJwtToken(token);
  } catch {
    return res.sendStatus(403);
  }

  next();
};

export default authRouter;
