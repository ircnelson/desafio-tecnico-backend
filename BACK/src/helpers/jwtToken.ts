import { sign, verify, JwtPayload } from "jsonwebtoken";
import { authSettings } from "../settings";

interface JwtContent {
  user: string;
}

interface JwtOptions {
  expiresIn: string | number;
}

export const generateJwtToken = (
  payload: JwtContent,
  options?: JwtOptions
): string => {
  return sign(payload, authSettings.jwtSecret, options);
};

export const verifyJwtToken = (jwt: string): string | JwtPayload => {
  return verify(jwt, authSettings.jwtSecret);
};
