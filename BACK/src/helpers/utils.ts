export const delay = (time: number): Promise<void> => {
  return new Promise((resolve) => setTimeout(resolve, time));
};

export enum LogOperation {
  Alter = "Alterar",
  Delete = "Remover",
}

export const createLogOperation = (operation: LogOperation) => {
  return (message: string) => {
    const now = new Date();

    return `${now.toLocaleDateString("pt-BR")} ${now.toLocaleTimeString("pt-BR")} - ${message} - ${operation.toString()}`;
  };
};
